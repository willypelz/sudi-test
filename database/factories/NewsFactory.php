<?php

namespace Database\Factories;

use App\Models\News;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;


class NewsFactory extends Factory {

    /*
    |--------------------------------------------------------------------------
    | Model Factories
    |--------------------------------------------------------------------------
    |
    | This directory should contain each of the model factory definitions for
    | your application. Factories provide a convenient way to generate new
    | model instances for testing / seeding your application's database.
    |
    */

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' =>  $this->faker->title,
            'content' => $this->faker->text,
            'user_id' => $this->faker->numberBetween(1, 7),
        ];
    }
}
