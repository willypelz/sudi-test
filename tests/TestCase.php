<?php

namespace Tests;

use App\Exceptions\Handler;
use App\Models\News;
use App\Models\User;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Http\JsonResponse;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $news;

    protected $headers;

    /**
     *
     * factory model connection for testing
     */
    public function setUp(): void
    {
        parent::setUp();
        /**
         * This disables the exception handling to display the stacktrace on the console
         * the same way as it shown on the browser
         */
        $this->disableExceptionHandling();

        \Artisan::call('migrate');
        \Artisan::call('passport:install');

        $user = User::factory()->create();
        Passport::actingAs($user);
        $token = $user->generateToken();
        $this->headers = [ 'Authorization' => 'Bearer $token'];
        $news = News::factory()->count(3)->create();
        $this->new = $news[0];
    }

    protected function disableExceptionHandling()
    {

    }

    }
