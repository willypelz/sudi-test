<?php

namespace Tests\Feature\Api\v1;

use App\Models\News;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;


class NewsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_success_when_creating_a_new()
    {
        $response = $this->postJson('/api/v1/news', NEWS, $this->headers);
        $response->assertStatus(JsonResponse::HTTP_CREATED)
            ->assertJson([
                "status_code" => JsonResponse::HTTP_CREATED,
                "status" => VARS['SUCCESS']
            ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_success_when_getting_all_news()
    {
        $response = $this->getJson('/api/v1/news', $this->headers);
        $response->assertStatus(JsonResponse::HTTP_OK)
            ->assertJson(["status_code" => JsonResponse::HTTP_OK]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_single_new_when_is_fetched()
    {
        $response = $this->getJson('/api/v1/news/' . $this->new->id, $this->headers);
        $response->assertStatus(JsonResponse::HTTP_OK)
            ->assertJson(["status_code" => JsonResponse::HTTP_OK]);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_success_when_new_was_updated()
    {
        $response = $this->patchJson('/api/v1/news/' . $this->new->id, NEWS, $this->headers);
        $response->assertStatus(JsonResponse::HTTP_OK)
            ->assertJson(["status_code" => JsonResponse::HTTP_OK]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_error_when_new_was_updated()
    {
        $response = $this->patchJson('/api/v1/news/' . 40, NEWS, $this->headers);
        $response->assertStatus(JsonResponse::HTTP_NOT_FOUND)
            ->assertJson(["error" => [
                "message" => "News to be updated not found",
                'status_code' => 404
            ]]);
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function it_returns_No_content_when_content_is_deleted()
    {

        $response = $this->deleteJson('/api/v1/news/' . $this->new->id, $this->headers);
        $response->assertStatus(JsonResponse::HTTP_OK)
            ->assertJson(["status_code" => JsonResponse::HTTP_NO_CONTENT]);
    }

}

const NEWS = [
    'title' => 'Sdui new application release',
    'content' => 'Sdui App is a cloud-based communication software that provides schools with
            tools to facilitate and streamline communication among teachers, students.',
    'user_id' => 5
];

const VARS = [
    "SUCCESS" => "success"
];
