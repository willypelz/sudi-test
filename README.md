
<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>  
  
<p align="center">  
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>  
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>  
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>  
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>  
</p>  
  
## Sdui News API Test(Laravel)
  
Laravel was used to develop the application because it is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:  
  
- [Simple, fast routing engine](https://laravel.com/docs/routing).  
- [Powerful dependency injection container](https://laravel.com/docs/container).  
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.  
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).  
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).  
- [Robust background job processing](https://laravel.com/docs/queues).  
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).  
  
Laravel is accessible, powerful, and provides tools required for large, robust applications.  
  
##  Project Setup
 In order to setup the application locally on you system. 
   1. clone the repository 
   2. `git clone git@gitlab.com:willypelz/sudi-test.git`  
   3. cd into the project directory 
   4. `cd sudi-test`
   5. install the dependencies for the application
   6. `composer install`
   7. create a .env file from the .env.example 
   8. `cp .env.example .env`
   9. Generate an application key
   10. `php artisan key:generate`
   11. Ensure `DB_DATABASE=SYSTEM_PATH/database.sqlite` the path of your system point to database.sqlite.
   12. update the env files with your mysql connection details that you have on your system
   13. Running migration data into the database
   14. `php artisan migrate`
   15. `php artisan passport:install`
   16. serving the project 
   17. `php artisan serve`
   18. `php artisan news:cleanup` this command is the one that clean up news older than 14days

##  Testing the Application 
**Application Testing**  is defined as a software  **testing**  type, conducted through scripts with the motive of finding errors in software. It deals with  **tests**  for the entire  **application**. It helps to enhance the quality of your  **applications**  while reducing costs, maximizing ROI, and saving development time.

In order to run the feature test that was written 
	`php ./vendor/bin/phpunit`
when you want to generate a coverage 
`php ./vendor/bin/phpunit --coverage-html ./coverage`

This generates html report files in the application in the coverage folder, which can be located in the root directory 

### Important 
Ensure you setup xdebug on your system. <small> [Xdebug Setup for local machine (xampp)](https://medium.com/d6-digital/installing-xdebug-for-xampp-with-php-in-windows-d2b750861118) </small>


## Note
<span style="color:red">Please note that the  `/` is for linux and mac terminal which applies to bash terminal also.
if you are using window command line with no bash you will have to use the `\`   </span>

## Test Coverage(Report) Overview

1. To view the test coverage 
2. navigate to the coverage folder 
3. click on the `index.html` open with any browser
4. To see risk report 
5. In the coverage folder 
6. click in the `dashboard.html` open with any browser (100% free from risk)

## Test application 

1. register or login if you have an account to have a token to use the application  `api/v1/login` or `api/v1/register`
2. use the token as a bearer key to access the news features API for the application 
  `` Authorization : Bearer TOKEN_GENERATED``

# Updates

1. There are still advance optimization and refactoring that can still be done in this project
2. Increase test case for news and also write test cases for users, login, register.


# Developer(Softwaredef)

1. Name: Asefon Michael Pelumi 
2. Nickname: Softwaredef
3. Mail: pelumiasefon@gmail.com

Thanks. If you have any problem setting it up or complain you can kindly post them on issues or message me directly
