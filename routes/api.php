<?php

use App\Http\Controllers\Api\v1\News\NewsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace('Api\v1')->prefix('v1')->group(function () {


    Route::group(['middleware' => ['cors', 'json.response']], function () {

        /*******************************************
         *****  News API Version 1  ***************
         *******************************************/
        Route::middleware('auth:api')->group(function () {
            Route::resource('news', '\App\Http\Controllers\Api\v1\News\NewsController');
        });

        /*******************************************
         *****  Auth API  ***************
         *******************************************/
        Route::post('/login', '\App\Http\Controllers\Auth\ApiAuthController@login')->name('login.api');
        Route::post('/register', '\App\Http\Controllers\Auth\ApiAuthController@register')->name('register.api');
        Route::post('/logout', '\App\Http\Controllers\Auth\ApiAuthController@logout')->name('logout.api');
    });
});
