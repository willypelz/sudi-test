<?php

namespace App\Console\Commands;

use App\Models\News;
use Illuminate\Console\Command;

class NewsCleanUpCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cleanup news that are no lonnger than 14 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Deleting News older than 14days...");
        News::where('created_at', '<', Carbon::now()->subDays(14))->delete();
        \Log::info("Deletion completed.");
        return 0;
    }
}
