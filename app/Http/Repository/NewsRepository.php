<?php

/************************************
 ** File: News Repository file  ******
 ** Date: 18th June 2022  ************
 ** News Repository file  ************
 ** Author: Asefon pelumi M. *********
 ** Senior Software Developer ********
 * Email: pelumiasefon@gmail.com  ***
 * **********************************/

namespace App\Http\Repository;


use App\Models\News;
use function GuzzleHttp\Promise\all;

class NewsRepository
{

    private $news;

    /**
     * NewsRepository constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }


    /**
     * functions to get all news
     *
     * @return News[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllNews($request, $limit)
    {
        return $this->news->filterBy($request)->with('user')->paginate($limit);
    }

//     get single news in the application

    /**
     * @param $table_field
     * @param $query
     * @return mixed
     */
    public function getSingleNews($table_field, $query)
    {
        return $this->news->where($table_field, $query)->first();
    }


    /**
     * function to update user details
     *
     * @param $request
     * @return News
     */
    public function updateNews($request, $id)
    {
        $news = $this->getSingleNews('id', $id);
        if (!$news) return 'News to be updated not found';
        $news->update($request->toArray());
        return $news;
    }


    /**
     * @param $query
     * @return mixed|string
     */
    public function searchNewsTable($query)
    {
        if (self::isSearchableFieldsSupplied($query)) {
            $key = array_key_first($query);
            return self::searchTableByColumn($key, $query[$key]);
        }
        return 'invalid search key supplied';
    }


    /**
     * @param $table
     * @param $query
     * @return mixed
     */
    public function searchTableByColumn($table, $query)
    {
        return $this->news->where($table, 'LIKE', "%$query%")->get();
    }

    /**
     * @param $table
     * @param $query
     * @return mixed
     */
    public function searchTableByDate($table, $query)
    {
        return $this->news->whereDate($table, $query)->get();
    }

    /**
     * @param $data
     * @return int
     */
    public function isSearchableFieldsSupplied($data)
    {
        return count(array_intersect(array_keys($data), News::$searchable_fields));
    }

}
