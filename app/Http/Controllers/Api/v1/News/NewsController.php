<?php

/************************************
 ** File: New Controller file  ******
 ** Date: 18th June 2022  ************
 ** New Controller file  ************
 ** Author: Asefon pelumi M. *********
 ** Senior Software Developer ********
 * Email: pelumiasefon@gmail.com  ***
 * **********************************/

namespace App\Http\Controllers\Api\v1\News;

use App\Http\Controllers\Controller;
use App\Http\Library\RestFullResponse\ApiResponse;
use App\Http\Repository\NewsRepository;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Http\Resources\v1\News\NewsResource;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    protected $newsRepository;
    protected $apiResponse;


    /**
     * NewsController constructor.
     * @param NewsRepository $newsRepository
     * @param ApiResponse $apiResponse
     */
    public function __construct(
        NewsRepository $newsRepository,
        ApiResponse    $apiResponse
    )
    {
        $this->newsRepository = $newsRepository;
        $this->apiResponse = $apiResponse;
    }

    /**
     * @group News management
     *
     *  New Collection
     *
     * An Endpoint to get all New in the system
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\New\NewsResourceCollection
     * @apiResourceModel \App\Models\New
     */
    public function index(Request $request)
    {
        try {
            $limit = ($request->has('_limit')) ? $request->get('_limit') : 10;
            $news = $this->newsRepository->getAllNews($request, $limit);
            return $this->apiResponse->respondWithDataStatusAndCodeOnly(
                NewsResource::collection($news), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return $this->apiResponse->respondWithError('Error getting news');
        }
    }


    /**
     * @group News management
     *
     *  News Collection
     *
     * An Endpoint to store news in the system
     *
     * @param CreateNewsRequest $request
     * @param News $news
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\New\NewsResourceCollection
     * @apiResourceModel \App\Models\New
     */
    public function store(CreateNewsRequest $request, News $news)
    {
        try {
            $request['user_id'] = $request->user()->id;
            $news = $news->create($request->toArray());
            return $this->apiResponse->respondWithDataStatusAndCodeOnly(
                new NewsResource($news), JsonResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->apiResponse->respondWithError('Error creating news');
        }
    }


    /**
     * @group News management
     *
     *  News Collection
     * An Endpoint to get single News details in the system
     *
     * @param News $news
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\New\NewsResourceCollection
     * @apiResourceModel \App\Models\New
     */
    public function show(News $news)
    {
        try {
            return $this->apiResponse->respondWithDataStatusAndCodeOnly(
                new NewsResource($news));
        } catch (\Exception $e) {
            return $this->apiResponse->respondWithError('Error getting news');
        }
    }

    /**
     * An Endpoint to update the specified resource from storage.
     *
     * @param UpdateNewsRequest $request
     * @return void
     */
    public function update(UpdateNewsRequest $request, $id)
    {
        try {
            $updatedNews = $this->newsRepository->updateNews($request, $id);
            if (is_string($updatedNews)) return $this->apiResponse->respondNotFound($updatedNews);
            return $this->apiResponse->respondWithNoPagination(
                new NewsResource($updatedNews),
                "The new $updatedNews->title was updated successfully");
        } catch (\Exception $e) {
            return $this->apiResponse->respondWithError('Error updating news');
        }
    }


    /**
     * An Endpoint to Remove the specified resource from storage.
     *
     * @param News $news
     * @return void
     * @throws \Exception
     */
    public function destroy(News $news)
    {
        try {
            $news->delete();
            return $this->apiResponse->respondDeleted("The news $news->title was deleted successfully");
        } catch (\Exception $e) {
            return $this->apiResponse->respondWithError('Error deleting news');
        }
    }

}
