<?php
/***********************************************
 ** File : CreateNewsRequest file
 ** Date: 18th June 2022  *********************
 ** CreateNewsRequest file
 ** Author: Asefon pelumi M. ******************
 ** Senior Software Developer ******************
 * Email: pelumiasefon@gmail.com  ***************
 * ***********************************************/

namespace App\Http\Requests;

use App\Library\Traits\ValidationTrait;
use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest
{

    use ValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'The title field is required',
            'content.required' => 'The content field is required',
        ];
    }
}
