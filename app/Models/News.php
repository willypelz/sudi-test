<?php
/***********************************************
 ** File : New Model file
 ** Date: 18th June 2022  *********************
 ** New Model file
 ** Author: Asefon pelumi M. ******************
 ** Senior Software Developer ******************
 * Email: pelumiasefon@gmail.com  ***************
 * ***********************************************/

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  use HasFactory;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'user_id'
    ];

    protected $dates = ['deleted_at'];

    public static $searchable_fields = [
        'title', 'content'
    ];

    public static function filterBy($request){
        return self::when($request->_title, function($query) use($request){
            return $query->where('title', "LIKE", "%{$request->_title}%");
        })->when($request->_body, function($query) use($request){
            return $query->where('content', "LIKE", "%{$request->_body}%");
        })->when($request->_key, function($query)  use($request){
            return $query->where('id', '=', $request->_key);
        })->when($request->user, function($query)  use($request){
            return $query->where('user_id', '=', $request->user);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
}
